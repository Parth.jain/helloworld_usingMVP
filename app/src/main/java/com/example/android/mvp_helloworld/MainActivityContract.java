package com.example.android.mvp_helloworld;

/**
 * Created by parth on 20/12/17.
 */

public interface MainActivityContract {

    interface View {

        void initView();

        void setViewData(String data);
    }

    interface Model {

        String getData();
    }

    interface Presenter {

        void onClick(android.view.View view);
    }

}
