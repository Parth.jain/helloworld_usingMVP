package com.example.android.mvp_helloworld;

import android.view.View;

/**
 * Created by parth on 20/12/17.
 */

public class MainActivityPresenter implements MainActivityContract.Presenter{
    private MainActivityContract.View mView;
    private MainActivityContract.Model mModel;

    public MainActivityPresenter(MainActivity view) {
        mView = (MainActivityContract.View) view;
        initPresenter();
    }

    private void initPresenter() {
        mModel = new MainActivityModel();
        mView.initView();
    }

    @Override
    public void onClick(android.view.View view) {
        String data = mModel.getData();
        mView.setViewData(data);
    }
}
